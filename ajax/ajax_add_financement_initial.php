<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once  ABSPATH . "config/dictionnary_error.php";
require_once  ABSPATH . 'modules/Data_Validation.php';
require_once  ABSPATH . 'interfaces/i_DB.php';
 
 
$validation = new Data_Validation();

$has_error = false;
// On lance les validations

if( ! $validation->verifText( $_POST['champ_nom'] ) ) { Error_Manager::getInstance()->addErrorInput(1, 'champ_nom'); $has_error = true; }
if( ! $validation->limitChara( $_POST['champ_nom'], 2, 50 ) ) { Error_Manager::getInstance()->addErrorInput(2, 'champ_nom'); $has_error = true; }

if( ! $validation->verifNumber( $_POST['champ_montant'] ) ) { Error_manager::getInstance()->addErrorInput(12, 'champ_montant'); $has_error = true; }
if( ! $validation->positifNumber( $_POST['champ_montant'], true ) ) { Error_manager::getInstance()->addErrorInput(12, 'champ_montant'); $has_error = true; }

$estPrevisionnelle = ( $_POST['estPrevisionnelle'] == 'true' ) ? 1 : 0;
$estUnBesoin = ( $_POST['financing_option'] == 'besoin' ) ? 1 : 0;

$date = '1996-11-14';

$msg = "Une erreur est survenue lors de l'enregistrement des données";
if( $has_error === false ) {
    $id_ecriture = add_ecriture_comptable( $_POST['champ_nom'], $_POST['account'], $_POST['champ_montant'], $date, $estPrevisionnelle, $_POST['entreprise'] );
    if( add_financement_initial( $id_ecriture, $estUnBesoin ) ) $msg = "La saisie a bien été enregistré";
}
die($msg);
 
