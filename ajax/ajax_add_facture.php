<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

$ajax = true;

require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'config/dictionnary_error.php';
require_once ABSPATH . 'interfaces/i_DB.php';
require_once ABSPATH . 'context/saisie_facture.php';
require_once ABSPATH . 'modules/member_area_manager.php';

$validation = new Data_Validation();

$has_error = false;
// On lance les validations
if( ! $validation->verifText( $_POST['wording'] ) )  $has_error = true;
if( ! $validation->limitChara( $_POST['wording'], 2, 50 ) )  $has_error = true;


if( ! $validation->verifNumber( $_POST['account'] ) ) $has_error = true;
if( ! $validation->limitChara( $_POST['account'], 2, 50 ) )  $has_error = true;


if( ! $validation->verifNumber( $_POST['amount'] ) )  $has_error = true;
if( ! $validation->limitChara( $_POST['amount'], 1, 8 ) )  $has_error = true;

$date = DateTime::createFromFormat( 'm/d/Y', $_POST['date']);
$date = $date->format("Y-m-d");

if ( $has_error === false ){
    if( add_ecriture_comptable( $_POST['wording'], $_POST['account'], $_POST['amount'], $date, false, $_POST['entreprise'] )) {
        die( "La facture a bien été ajouté");
    }
    else {
        die("Erreur lors de l'ajout de la facture");
    }
}
else {
    die( "Erreur lors de l'ajout de la facture");
}