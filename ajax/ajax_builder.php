<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . "modules/Form_Manager.php";
require_once ABSPATH . "interfaces/i_DB.php";
require_once ABSPATH . "modules/member_area_manager.php";


if( isset( $_POST['id_user'] ) ) {
    // On définit les valeurs pas défauts
    if( ! isset( $_POST['id_entreprise'] ) )
        $_POST['id_entreprise'] = get_entreprises( $_POST['id_user'] )[0]['ID'];
    
    if( ! isset( $_POST['field'] ) )
        $_POST['field'] = array_keys( get_account( get_SIREN_entreprise( $_POST['id_entreprise'] ) ) )[0];
    
    if( ! isset( $_POST['type_presentation'] ) )
        $_POST['type_presentation'] = 'chart';
    
    if( ! isset( $_POST['type_tuile'] ) )
        $_POST['type_tuile'] = 'suivi';
    
    if( ! isset( $_POST['type_data'] ) )
        $_POST['type_data'] = 'compte';
    
    if( !isset($_POST['changement']) )
        $_POST['changement'] = false;
    
    if( !isset( $_POST['date_debut_periode'] ) )
        $_POST['date_debut_periode'] = false;
    if( !isset( $_POST['date_fin_periode'] ) )
        $_POST['date_fin_periode'] = false;
    
    if( !isset( $_POST['date_debut_premiere_periode'] ) )
        $_POST['date_debut_premiere_periode'] = false;
    if( !isset( $_POST['date_fin_premiere_periode'] ) )
        $_POST['date_fin_premiere_periode'] = false;
    
    if( !isset( $_POST['date_debut_seconde_periode'] ) )
        $_POST['date_debut_seconde_periode'] = false;
    if( !isset( $_POST['date_fin_seconde_periode'] ) )
        $_POST['date_fin_seconde_periode'] = false;
    if( !isset( $_POST['submit'] ) )
        $_POST['submit'] = false;
    
    
/************************ tableau de valeur des selects ************************/
    $arg_fourchette_simple = array(
        "debut" => array(
            "label" => "Début : ",
            "name" => "debut-periode",
            "id_html" => "debut-periode",
            "class_html" => "date-picker",
            'value' => $_POST['date_debut_periode']
        ),
        "fin" => array(
            "label" => "Fin : ",
            "name" => "fin-periode",
            "id_html" => "fin-periode",
            "class_html" => "date-picker",
            'value' => $_POST['date_fin_periode']
        ),
    );

    $Type_de_presentation = array(
        'chart' => 'Graphique',
        'clacos' => 'Camembert',
        'tableaux' => 'Tableaux',
        'frise' => 'Frise'
    );

    // On remplis un tableau avec la liste des entreprises de l'utilisateur
    $entreprises = get_entreprises( $_POST['id_user'] );
    $liste_entreprise = array();
    foreach ($entreprises as $single) {
        $liste_entreprise[ $single['ID'] ] = $single['nom'];
    }

    $type_data = array(
        'compte' => 'Compte' ,
        'calcul' => 'Calcul',
        'total' => 'Total',
    );

    $type_tuile= array(
        'suivi' => "suivi",
        'comparaison' => 'Comparaison',
    );
    
    
    $form = new Form_Manager('#', '', 'post', 'form-builder');

    /***************************** INITIALISATION *****************************/
    
    // On affiche le champ select de l'entreprise
    $form->TDBF_Display_select(
        $liste_entreprise,
        'Entreprise concernée',
        'id-entreprise',
        'select',
        false,
        $_POST['id_entreprise']
    );

    // On affiche le champ select du type de prés$arg_fourchette_simpleentation
    $form->TDBF_Display_select(
        $Type_de_presentation, 
        'Type de présentation', 
        'type-presentation',
        'select',
        false,
        $_POST['type_presentation']
    );
    
    // On affiche le champ select du type tuile
    $form->TDBF_Display_select(
        $type_tuile, 
        'Type de tuile', 
        'type-tuile',
        'select',
        false,
        $_POST['type_tuile']
    );
    
    // On affiche le champ select du type_data
    $form->TDBF_Display_select(
        $type_data, 
        'Type de données', 
        'type-data',
        'select',
        false,
        $_POST['type_data']
    );
    
    /**************************************************************************/
    if( $_POST['type_tuile'] === 'comparaison' ){
        // c'est une une comparaison
        // On affiche le champ select du type_data
        $type_comparaison = array(
            'previ-reel' => "Le prévisionnelle et le réel",
            "deux-periodes" => "Deux periodes"
        );
        
        if( !isset($_POST['type_comparaison']) )
            $_POST['type_comparaison'] = 'previ-reel';
        
        $form->TDBF_Display_select(
            $type_comparaison, 
            'Comparaison entre : ', 
            'type-comparaison',
            'select',
            false,
            $_POST['type_comparaison']
        );
        comparaison( );
    }
    else {
        // c'est pas une comparaison
        display_liste_field();
        display_fourchette( $arg_fourchette_simple );
        display_submit();
    }
    
    
    
    ob_start();
//    var_dump( $_POST );
    $form->display();
    
    die( ob_get_clean() );
    
    
}

function comparaison( ) {
    global $arg_fourchette_simple;
    
    switch ( $_POST['type_comparaison'] ) {
        case 'previ-reel':
            display_liste_field();
            display_fourchette( $arg_fourchette_simple );
            display_submit();
            break;
        case 'deux-periodes':
            display_liste_field();
            display_deux_periodes();
            display_submit();
            break;
        default:
            trigger_error( "Type de comparaison inconnus" );
            break;
    }
}

function display_liste_field( ) {
    $fonction = "display_liste_" . $_POST['type_data'];
    if ( ! function_exists( $fonction ) ) trigger_error ( "Field incorect", E_USER_ERROR ) ;
    
    $fonction();
}

function display_deux_periodes() {
    global $form;
    
    $arg_premiere_periode = array(
        "debut" => array(
            "label" => "Début de la première période : ",
            "name" => "debut-premiere-periode",
            "id_html" => "debut-premiere-periode",
            "class_html" => "date-picker",
            "value" => $_POST['date_debut_premiere_periode']
        ),
        "fin" => array(
            "label" => "Fin de la première période : ",
            "name" => "fin-premiere-periode",
            "id_html" => "fin-premiere-periode",
            "class_html" => "date-picker",
            "value" => $_POST['date_fin_premiere_periode']
        ),
    );
    $arg_seconde_periode = array(
        "debut" => array(
            "label" => "Début de la seconde période : ",
            "name" => "debut-seconde-periode",
            "id_html" => "debut-seconde-periode",
            "class_html" => "date-picker",
            "value" => $_POST['date_debut_seconde_periode']
        ),
        "fin" => array(
            "label" => "Fin de la seconde période : ",
            "name" => "fin-seconde-periode",
            "id_html" => "fin-seconde-periode",
            "class_html" => "date-picker",
            "value" => $_POST['date_fin_seconde_periode']
        ),
    );
    display_fourchette( $arg_premiere_periode );
    display_fourchette( $arg_seconde_periode );
   
}

function display_fourchette( $arg ) {
    global $form;
    
  
    // début de la fourchette
    $form->TDBF_Display_date_input(
        $arg['debut']['label'], 
        $arg['debut']['name'], 
        $arg['debut']['id_html'],
        $arg['debut']['class_html'],
        $arg['debut']['value']
    );
    // fin de la fourchette
    $form->TDBF_Display_date_input(
        $arg['fin']['label'], 
        $arg['fin']['name'], 
        $arg['fin']['id_html'],
        $arg['fin']['class_html'],
        $arg['fin']['value']
    );
}


function display_submit(){
    global $form;
    
    $form->TDBF_Display_button(
        'envoyer',
        'envoyer',
        'Enregistrer',
        'submit'    
    );
}    
    
function display_liste_compte() {
    global $form;
    
    $compte = get_account( get_SIREN_entreprise( $_POST['id_entreprise'] ) );

    if( ! in_array( $_POST['field'], $compte ) && $_POST['changement'] != 'field' ) {
        // Si le field ne correspond pas à un compte
        // La valeur par defaut est le premier compte
        $_POST['field'] = array_keys( $compte )[0];
    }
    
    $form->TDBF_Display_select(
        $compte, 
        'Le compte à surveiller', 
        'field',
        'select',
        false,
        $_POST['field']
    );
}

function display_liste_calcul() {
    global $form;
    
    $calcul = array(
        'bfr' => 'Besoin en Fond de Roulement',
        'seuille-rentabilite' => 'Seuille de Rentabilité',
    );
    
    if( ! in_array( $_POST['field'], $calcul ) && $_POST['changement'] != 'field' ) {
        // Si le field ne correspond pas à un calcul
        // La valeur par defaut est le premier calcul
        $_POST['field'] = array_keys( $calcul )[0];
    }
    
    $form->TDBF_Display_select(
        $calcul, 
        'Nom du calcul', 
        'field',
        'select',
        false,
        $_POST['field']
    );
}

function display_liste_total() {
    global $form;
    
    $total = array(
        'encaissement-mensuel' => "Total encaissement mensuel",
        'decaissement-mensuel' => "Total décaissement mensuel",
        'masse-salarial' => 'Total masse salarial',
        'compte' => "Total d'un compte mensuel",
    );
    
    if( ! in_array( $_POST['field'], $total ) && $_POST['changement'] != 'field' ) {
        // Si le field ne correspond pas à un total
        // La valeur par defaut est le premier total
        $_POST['field'] = array_keys( $total )[0];
    }
    
    $form->TDBF_Display_select(
        $total, 
        "Choix du total", 
        'field',
        'select',
        false,
        $_POST['field']
    );
}


?>











/*
if( isset( $_POST['step'] ) && isset( $_POST['id_user'] ) ):
ob_start();

//var_dump($_POST);

switch ($_POST['step']) {
/**************************** DEBUT INITIALISATION ****************************
    case 'initialisation':

        $Type_de_presentation = array(
            'chart' => 'Graphique',
            'clacos' => 'Camembert',
            'tableaux' => 'Tableaux',
            'frise' => 'Frise'
        );
        $type_data = array(
            'liste-compte' => 'Compte' ,
            'liste-calcul' => 'Calcul',
            'liste-comparaison' => 'Comparaison',
            'liste-total' => 'Total',
        );

        $form_initial = new Form_Manager( '#', '', 'POST', 'form-initial' );

        // On remplis un tableau avec la liste des entreprises de l'utilisateur
        $entreprises = get_entreprises( $_POST['id_user'] );
        $liste_entreprise = array();
        foreach ($entreprises as $single) {
            $liste_entreprise[ $single['ID'] ] = $single['nom'];
        }
        $form_initial->TDBF_Display_select(
            $liste_entreprise,
            'Entreprise concernée',
            'entreprise',
            'class'
        );

        $form_initial->TDBF_Display_select(
            $Type_de_presentation, 
            'Type de présentation', 
            'Type_de_presentation', 
            'Type_de_presentation'
        );
        $form_initial->TDBF_Display_select(
            $type_data, 
            'Type de données', 
            'type_data', 
            'type_data'
        );

        $form_initial->display();
    break;
/***************************** DEBUT LISTE-COMPTE *****************************
    case 'liste-compte':
        $form_type_data = new Form_Manager('#', '', 'POST', 'form-type-data' );
        if( isset( $_POST['id_entreprise'] ) ){
            $compte = get_account(get_SIREN_entreprise( $_POST['id_entreprise'] ) );
            $form_type_data->TDBF_Display_select($compte, 'Le compte à surveiller', 'account', '');
        }
        else echo "<p class='error'>Erreur, veuillez raffraichir la page</p>";
        $form_type_data->display();
    break;
/***************************** DEBUT LISTE-CALCUL *****************************
    case 'liste-calcul':
        $form_type_data = new Form_Manager('#', '', 'POST', 'form-type-data' );
        $calcul = array(
            'bfr' => 'Besoin en Fond de Roulement',
        );
        $form_type_data->TDBF_Display_select($calcul, 'Nom du calcul', 'calcul', '');
        $form_type_data->display();
    break;
/*************************** DEBUT LISTE-COMPARAISON ***************************
    case 'liste-comparaison':
        $form_type_data = new Form_Manager('#', '', 'POST', 'form-type-data' );
        $comparaison = array(
            'liste-compte' => "Le prévisionnelle et le réel d'un compte",
            'liste-total' => "Le prévisionnelle et le réel d'un total",
            'liste-calcul' => "Le prévisionnelle et le réel d'un calcul",
            'deux-periodes-compte' => "Un compte sur deux périodes différentes",
            'deux-periodes-total' => "Un total sur deux périodes différentes",
            'deux-periodes-calcul' => "Un calcul sur deux périodes différentes",
            
            
        );
        $form_type_data->TDBF_Display_select( $comparaison, "Type de comparaison", 'comparaison', '');
        $form_type_data->display();
    break;
/****************************** DEBUT LISTE-TOTAL ******************************
    case 'liste-total':
        $form_type_data = new Form_Manager('#', '', 'POST', 'form-type-data' );
        $total = array(
            'encaissement-mensuel' => "Total encaissement mensuel",
            'decaissement-mensuel' => "Total décaissement mensuel",
            'masse-salarial' => 'Total masse salarial',
            'compte' => "Total d'un compte mensuel",
        );
        $form_type_data->TDBF_Display_select( $total, "Choix du total", 'total', '');
        $form_type_data->display();
    break;

/*********************************** DEFAULT ***********************************
    default:
        echo "Erreur : Veuillez raffraichir la page";
    break;
/******************************** FIN DU SWITCH ********************************
}
die( ob_get_clean() );




endif;
*/