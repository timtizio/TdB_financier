<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname( dirname(__FILE__) ) ) . '/');
//include "../../modules/Data_validation.php";
include ABSPATH . "modules/Data_Validation.php";

use PHPUnit\Framework\Assert;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context{
    private $validationClass;
    
    private $value;
    
    public function __construct() {
        $this->validationClass = new Data_Validation();
    }
    
    /**
     * @when quand on valide :value avec le test VerifTexte
     */
    public function quandOnValideAvecTestTexte( $value ) {
        $this->value = $this->validationClass->verifText( $value );
    }
    /**
     * @when quand on valide :value avec le test VerifNumber
     */
    public function quandOnValideAvecTestNumber( $value ) {
        $this->value = $this->validationClass->verifNumber( $value );
    }
    
    /**
     * @when quand on valide :value avec le teste limite de caractére qui est limité de :min à :max caractéres
     */
    public function quandOnValideAvecTestLimitChara( $value, $min, $max ) {
        $this->value = $this->validationClass->limitChara( $value, $min, $max );
    }
    
    /**
     * @when quand on valide :value avec le test de validation du nombre qui est limité de :min à :max
     */
    public function quandOnValideAvecTestNumberMinMax( $value, $min, $max ) {
        $this->value = $this->validationClass->numberMinMax( $value, $min, $max );
    }
    
    /**
     * @when on séléctionne le jours :value avec le test de validation du choix jours
     */
    public function quandOnValideAvecTestVerifDay( $value ) {
        $this->value = $this->validationClass->verifDays( $value );
    }
    
    /**
     * @when on séléctionne le mois :value avec le test de validation du choix mois
     */
    public function quandOnValideAvecTestVerifMonth( $value ) {
        $this->value = $this->validationClass->verifMonth( $value );
    }
    
    /**
     * @when on séléctionne l'année :value avec le test de validation de l'année
     */
    public function quandOnValideAvecTestVerifYears( $value ) {
        $this->value = $this->validationClass->verifYears( $value );
    }

    /**
     * @when on séléctionne la date :value avec le test de validation des dates
     */
    public function quandOnValideAvecTestDate( $value ) {
        $this->value = $this->validationClass->verifDate( $value );
    }
    
    /**
     * @when on valide :value et :estPositif dans le test de validation des nombres positifs negatifs
     */
    public function quandOnValideAvecTestPositifNumber( $value, $estPositif ) {
        $this->value = $this->validationClass->positifNumber( $value, $estPositif );
    }
    
    /**
     * @then une validation est envoyée et elle retourne true
     */
    public function uneValidationEstEnvoyéeEtRetourneTrue() {
        Assert::assertTrue(
            $this->value
        );
    }
    
    /**
     * @then une validation est envoyée et elle retourne false
     */
    public function uneValidationEstEnvoyéeEtRetourneFalse() {
        Assert::assertFalse(
            $this->value
        );
    }
    
    
}
