<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

require_once ABSPATH . "modules/Error_Manager.php";
require_once ABSPATH . "config/config.php";
if(ERROR_IN_LOG) set_error_handler( 'my_handler' );

require_once ABSPATH . "config/url_manager.php";
require_once ABSPATH . "modules/member_area_manager.php";

session_start();

// rajouter un / a la ligne ci-dessous pour se déconnecter
    /*
session_unset ();
session_destroy ();

    //*/

ob_start();
if(!empty($_GET['p']) && array_key_exists($_GET['p'], $page) ){
    
    $name = $_GET['p'];
    
    if( ! is_connected() && $page[$name]['visibility'] === 'private') {
        // Si on veut accéder à une page private et qu'on est pas connecter
        require ABSPATH . 'context/connection.php';
    }
    else {
        $currentPage = $page[$name]['file-name'];
        $title = $page[$name]['title'];
        require ABSPATH . 'context/'.$currentPage;
    }
     
} else {
    require ABSPATH . 'public/home.php';
}

$content = ob_get_clean();

require ABSPATH . 'public/template.php';

// On écris les lignes de log
Error_Manager::getInstance()->logError();

function my_handler(  $errno, $errstr, $errfile, $errline, $errcontext ) {
    $errManager = Error_Manager::getInstance();
    
    $errManager->addErrorPHP( $errno, $errstr, $errfile, $errline );
}
