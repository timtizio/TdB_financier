<?php
$dictionnary = array(
    0 => "Erreur Iconnu",
    1 => "Le champ nom n'accepte que des lettres",
    2 => "Le nombre de caractère du champ doit être compris entre 2 et 50",
    3 => "Le champ email est incorect",
    4 => "Le nombre de caractère du champ doit être compris entre 2 et 100",
    5 => "Le nombre de caractère du champ mot de passe doit être compris entre 8 et 20",
    6 => "Les mots de passes ne sont pas identique",
    7 => "Un numero SIRET ne doit contenir que des chiffres",
    8 => "Un numero SIREN ne doit contenir que des chiffres",
    9 => "Un numero SIRET contient 14 chiffres",
    10 => "Un numero SIREN contient 9 chiffres",
    11 => "L'adresse mail ou le mot de passe est incorect",
    12 => "Le montant doit être positif et supérieur à 0€",
    13 => "Le libellé ne peut contenir que des lettres",
    14 => "Compte incorect",
    15 => "Le nom de votre investissement ne doit contenir que des lettre",
    16 => "Le nom de votre investissement est limité a 50 charactères",
    17 => "Le montant de votre investissement ne doit contenir que des chifres",
    18 => "Le montant de votre investissement est limité a 8 chifres",
    19 => "Le pourcentage de TVA ne doit contenir que des chifres",
    20 => "Le pourcentage de TVA est limité a 3 chifres",
    21 => "La durée saisie est limité a 2 chifres",
    22 => "La durée saisie ne doit contenir que des chifres",
    23 => "Cette adresse mail est déjà utilisé",

);

/**
 * Renvoit le message associé au code erreur $code
 * 
 * @param int $code Le ctypeode du message
 * @return string Le message associé au code $code
 */
function getMessage( $code ) {
    global $dictionnary;
    
    if( isset( $dictionnary[ $code ] ) ) {
        return $dictionnary[ $code ];
    }
    else {
       return $dictionnary[0]; 
    }
    
}