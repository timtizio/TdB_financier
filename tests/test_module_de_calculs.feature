Feature: tdb_financier calculs

Dans un premier temps le module récupère les données depuis la DB.
Ensuite suivant les paramètres il choisit un calcul à effectuer.
Puis il effectue le calcul.
et Enfin il retourne le resultat du calcul.

Scénario: Calcul de la somme d'un tableau de nombres 2 3 4
    When quand la fonction calcul_sum()| 2 | 3 | 4 | est appellée
    Then Resultat de la somme des montant du tableau d'entrée est = à "9" et il est enregistré

Scénario: Calcul de la différence entre prévisionnel et réel 
    When quand la fonction calcul diff() est appellé  "500" réel "400" prévi
    Then Le resultat "100" est renvoyé et enregistré 

Scénario: Calcul d'un pourcentage en fonction de deux valeurs
    When lorsque la fonction percent() est appellée "10" et "5" 
    Then le resultat "50" est renvoyé et enregistré

Scénario: Calcul de la somme d'un tableau de nombres
    When quand la fonction calcul_sum()"u" est appellée
    Then renvoi une erreur et arrète le script

Scénario: Calcul de la différence entre prévisionnel et réel 
    When quand la fonction calcul diff() est appellé  "u" réel "400" prévi
    Then renvoi une erreur et arrète le script

Scénario: Calcul d'un pourcentage en fonction de deux valeurs
    When lorsque la fonction percent() est appellée "u" et "5" 
    Then renvoi une erreur et arrète le script

Scénario: Calcul de la somme d'un tableau de nombres 
    When quand la fonction calcul_sum()"9" est appellée
    Then renvoi une erreur et arrète le script

Scénario: Calcul de la différence entre prévisionnel et réel 
    When quand la fonction calcul diff() est appellé  | 400 | 100 | réel "400" prévi
    Then renvoi une erreur et arrète le script

Scénario: Calcul de la somme d'un tableau de nombres 2 3 4
    When quand la fonction calcul_sum()| 2.55 | 3.45 | 4 | est appellée
    Then Resultat de la somme des montant du tableau d'entrée est = à 10 et il est enregistré

Scénario: Calcul de la différence entre prévisionnel et réel 
    When quand la fonction calcul diff() est appellé  "500.55" réel "400.50" prévi
    Then Le resultat "100.05" est renvoyé et enregistré 

Scénario: Calcul d'un pourcentage en fonction de deux valeurs
    When lorsque la fonction percent() est appellée 5 | 5 | et "5" 
    Then le resultat "50" est renvoyé et enregistré

Scénario: Calcul de la somme d'un tableau de nombres 2 3 4
    When quand la fonction calcul_sum()| - 2 | - 3 | - 4 | est appellée
    Then Resultat de la somme des montant du tableau d'entrée est = à "-9" et il est enregistré

Scénario: Calcul de la différence entre prévisionnel et réel 
    When quand la fonction calcul diff() est appellé  "500" réel "1000" prévi
    Then le resultat "-500" est renvoyé et enregistré

Scénario: Calcul d'un pourcentage en fonction de deux valeurs
    When lorsque la fonction percent() est appellée "-500" et "50" 
    Then renvoi une erreur et arrète le script