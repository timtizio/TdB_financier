<?php

/**
 * Description of Display_management
 * 
 * @author joHNkHaN_scr
 */



/**
 * @author Timothé Tizio-Menut and joHNkHaN
 * 
 * @param array $arg reçois un tableau bidimentionnel pour passer les élément à afficher
 * @return array Un tableau bidimentionnel
 */
function display_array( $arg, $id = '' ) {
    if ( !is_array($arg[0]) ) {
        trigger_error("your param entry is not a two-dimensional array", E_USER_ERROR);
    }
    
    if ( $id != '' ) $id = "id='" . $id . "'";
?>

    
    <section <?= $id ?>>
        <table>
            <?php foreach($arg as $line): ?>
            <tr>
                <?php foreach($line as $td): ?>
                <td><?php echo $td; ?></td>
                <?php endforeach; ?>
            </tr>
            <?php endforeach; ?>

        </table>
    </section>

<?php
}
