<?php

/* 
 * fonction retournant un booléen si l'utilisateur est connecté
 * 
 * @return  bool                vrai ou faux
 */
function is_connected(){
    $value_return = false;
    if (isset($_SESSION['id_user'])){
        $value_return = true;
    }
    return $value_return;
}

/**
 * 
 * fonction de deconnexion de l'utilisateur
 * 
 */
function deconnection(){
    // suppression des variables de session
    session_unset ();
    //destruction de la session
    session_destroy ();
    //supression des variables de cookie
    //unset($_COOKIE['mot de passe haché']);
}

/**
 * 
 * fonction de connexion de l'utilisateur
 * 
 * @param $id               id de l'utilisateur
 * @param $password         mot de passe haché de l'utilisateur 
 */
function connection( $id ){
    //demarage de la session => DÉJA FAIT DANS L'INDEX
    //session_start(); 
    //création des variables de session
    $_SESSION['id_user'] = $id; 
    //envoi des cookie
    //if (!isset($_COOKIE)){
    //setcookie("mot_de_passe_hache",$password,time()+3600*24*31);
    //setcookie("id_user",$id,time()+3600*24*31);
    //}
}

function get_ID_user() {
    $val_return = false;
    if( is_connected() ) $val_return = $_SESSION['id_user'];
    
    return $val_return;
}
