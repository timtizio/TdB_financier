<?php

class Chart_Manager {


    /**
    * Genere un graphique simple, de type bar ou line avec un seul groupe de donnée
    * Les array $xLabel et $data doivent etre de taille identique
    *
    * @param string $type, line, radar ou bar
    * @param string $nom, Nom du graphique
    * @param array $xLabel, parametre de l'axe X du graphique, chaque valeur est associé à celle de $data
    * @param array $data, donneé du graphique
    * @param string $chartID, id du canvas html contenant le graphique
    *
    * @return string $jsChart, code js qui genere le graphique
    */
    public function simpleChart($type, $nom, $xLabel, $data, $chartId){
       
        if(count($xLabel) == count($data)){
            $xLabel = json_encode($xLabel);
            $data = json_encode($data);

          $jsChart = "    var ctx2 = document.getElementById(\"".$chartId."\");
        var myChart2 = new Chart(ctx2, {
            type: '". $type ."',
            data: {
                labels: ". $xLabel .",
                datasets: [{
                    label: '".$nom."',
                    data: ".$data.",
                    backgroundColor: 'rgba(153,255,51,0.4)'
                    }]
                }
            });";

        return $jsChart;
        } else {
            echo '</script>';
            trigger_error('probleme graphique', E_USER_NOTICE);
         }
        
    }


    /**
    * Genere un graphique double, de type bar ou line avec 2 groupes de donnée à comparer
    * Les array $data1 et $data2 doivent etre de même taille que l'array xlabel
    *
    * @param string $type, line, radar ou bar
    * @param string $nom1, Nom du premier graphique
    * @param string $data1, donnée numerique du premier graphique
    * @param array $xlabel, parrametre de l'axe X du graphique, chaque valeur est associé à celle de $data1 et $data2
    * @param string $nom2, Nom du second graphique
    * @param array $data2, donnée numerique du second graphique
    * @param string $chartId, id du canvas html contenant le graphique
    *
    * @return string $jsChart, code js qui genere le graphique
    *
    */
    public function doubleChart($type, $nom1, $data1, $xlabel1, $nom2, $data2, $chartId){

        if ( count($xlabel1) == count($data1) AND count($xlabel1) == count($data2) ){
            $xlabel1 = json_encode($xlabel1);
            $data1 = json_encode($data1);
            $data2 = json_encode($data2);

            $jsChart = "var ctx = document.getElementById(\"".$chartId."\").getContext('2d');
            var myChart = new Chart(ctx, {
              type: '". $type ."',
              data: {
                labels: ". $xlabel1 .",
                datasets: [{
                  label: '".$nom1."',
                  data: ". $data1 .",
                  backgroundColor: 'rgba(153,255,51,0.4)'
                }, {
                  label: '".$nom2."',
                  data: ". $data2 .",
                  backgroundColor: 'rgba(255,153,0,0.4)'
                }]
              }
            });";

            return $jsChart;
            
        } else {
            echo '</script>';
            trigger_error('probleme graphique', E_USER_NOTICE);
         }

    }

    /**
    * Genere un graphique de type pie, doughnut, ou polarArea
    * Les array $data et xLabel doivent etre de la même taille afin de ne pas avoir de donnée undefind à la generation
    *
    * @param string $type, pie, doughnut ou polarArea
    * @param array $xLabel, correspond au nom des donnes de $data
    * @param array $data, donne numerique du graphique, ne doit pas depasser 11 donnée afin que le graphique se genere correctemen
    * @param $chartId, id du canvas html contenant le graphique
    *
    * @return string $jsChart, code js qui genere le graphique
    *
    */
    public function pieChart($type, $xLabel, $data, $chartId){
        
        if(count($xLabel) == count($data)) {

            $color = array(
                0 => '#2ecc71',
                1 => '#3498db',
                2 => '#95a5a6',
                3 => '#9b59b6',
                4 => '#f1c40f',
                5 => '#e74c3c',
                6 => '#34495e',
                7 => '#8bc34a',
                8 => '#00acc1',
                9 => '#ff5722',
                10 => '#c62828',
            );

            $totalData = count($data);
            $totalColor = count($color);

            if($totalData <= $totalColor){
                
                if($totalData != $totalColor){

                    $diff = $totalColor - $totalData;

                    $color = array_slice($color, $diff, $totalData);

                }

            $data = json_encode($data);
            $color = json_encode($color);
            $xLabel = json_encode($xLabel);

            $jsChart = "var ctx3 = document.getElementById(\"". $chartId ."\").getContext('2d');
            var myChart3 = new Chart(ctx3, {
            type: '". $type ."',
            data: {
                labels: ". $xLabel .",
                datasets: [{
                backgroundColor: ". $color .",
                data: ". $data ."
                }]
            }
            });";

            }
            return $jsChart;
        } else {
           echo '</script>';
           trigger_error('probleme graphique', E_USER_NOTICE);
        }

    }
}