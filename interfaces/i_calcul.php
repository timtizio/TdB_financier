<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . "modules/calcul_manager.php";

/**
 * fonction retournant le résultat d'un calcul au choix
 * 
 * @param string $calcul_name           nom du calcul à faire
 * @param array $array                  tableau de données
 * @param float $value                  valeur d'un compte
 * @param float $valueA                 valeur compte prévi
 * @param float $valueB                 valeur compte réel
 */
function make_calcul( $calcul_name, $array = null, $value = null ,$valueA = null, $valueB = null ) {
    switch ( $calcul_name ){
        case ( $calcul_name == "sum" ):
            $result = calcul_sum( $array );
            $r = $result;
            break;
        case ( $calcul_name == "diff" ):
            $result = calcul_diff( $valueA, $valueB );
            $r = $result;
            break;
        case ( $calcul_name == "percent" ):
            $result = calcul_percent( $array, $value );
            $r = $result;
            break;
        default:
            $r = false;
            break;
    }
    return $r;
}
