var select_entreprise = $("#entreprise");

select_entreprise.change(function() {
    $.post(
        'ajax/ajax_update_account.php', // Le fichier cible côté serveur.
        { ID_entreprise : select_entreprise.val() },
        endRequest, // Nous renseignons uniquement le nom de la fonction de retour.
        'text' // Format des données reçues.
    );
});

function endRequest(texte_recu) {
//    console.log("FIN DE LA REQUETE");
//    console.log("RESULTAT : " + texte_recu);
    
    $("#account").children().remove();
    $("#account").append(texte_recu);
}