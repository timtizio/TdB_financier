<?php

require_once ABSPATH . "modules/Error_Manager.php";
require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'interfaces/i_DB.php';

function display_form_supp_entreprise() {
    $form = new Form_Manager('#', 'class', 'post');

    // On remplis un tableau avec la liste des entreprises de l'utilisateur
    $entreprises = get_entreprises( get_ID_user() );
    
    $liste_entreprise = array();
    foreach($entreprises as $single) {
        $liste_entreprise[ $single['ID'] ] = $single['nom'];
    }

    $id_entreprise = $entreprises[0]['ID'];

    $form->TDBF_Display_select( $liste_entreprise, 'Entreprise concernée', 'entreprise', 'class', Error_Manager::getInstance()->getErrorInput( 'entreprise' ));
    $form->TDBF_Display_select( liste_membres_entreprise( $id_entreprise ), 'Utilisateur concernée', 'user', 'class', Error_Manager::getInstance()->getErrorInput( 'user' ));
    $form->TDBF_Display_button ( 'send', 'class', 'Supprimer', 'submit' );
   
    $form->display();
}


//Si on clique sur Supprimer
if(isset($_POST['send']) AND !empty($_POST['entreprise']) AND !empty($_POST['user'])){
    
    $id_user = $_POST['user'];
    supp_liaison( $id_user );   
    supp_ecriture_comptable( $id_user );
    supp_plan_comp_perso( $id_user );
    supp_entreprise( $id_user );
}

display_form_supp_entreprise();