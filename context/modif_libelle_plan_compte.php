<?php

require_once ABSPATH . 'modules/Error_Manager.php';
require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'modules/member_area_manager.php';
require_once ABSPATH . 'interfaces/i_DB.php';
require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'public/js/Load_Script.php';

Load_Script::getInstance()->enqueue_script( 'public/js/update_account.js' );
if( isset($_POST['send']) && !empty($_POST['wording']) ){
    $validation = new Data_Validation();
    $has_error = false;
    if( ! $validation->verifText( $_POST['wording'] ) ) { Error_Manager::getInstance()->addErrorInput(13, 'wording'); $has_error = true; }
    
    $message = "Le libellé n'a pas pu être changé";
    if (! $has_error) {
        if( set_label_account( $_POST['entreprise'], $_POST['account'], $_POST['wording'] ) ) {
            $message =  "Le libellé à bien été changé";
        }
    }
    echo $message;
}
display_form_modif_libelle_compte();







function display_form_modif_libelle_compte() {
    $form = new Form_Manager('#', 'class', 'post');

    // On remplis un tableau avec la liste des entreprises de l'utilisateur
    $entreprises = get_entreprises( get_ID_user() );
    $liste_entreprise = array();
    foreach($entreprises as $single) {
        $liste_entreprise[ $single['ID'] ] = $single['nom'];
    }
    $form->TDBF_Display_select( $liste_entreprise, 'Entreprise concernée', 'entreprise', 'class', Error_Manager::getInstance()->getErrorInput( 'entreprise' ));
    
    // A MODIFIER !!!!!!!
    $SirenEntreprise = $entreprises[0]['SIREN'];
    //$SirenEntreprise = '987654321';
    /////////////////////
    
    $form->TDBF_Display_select ( get_account( $SirenEntreprise ) ,'Compte', 'account', 'class','', Error_Manager::getInstance()->getErrorInput( 'account' ) );
    $form->TDBF_Display_text ( 'Nouveau libellé', 'wording', 'class','',Error_Manager::getInstance()->getErrorInput( 'wording' ));
    $form->TDBF_Display_button ( 'send', 'class', 'Envoyer', 'submit' );
    $form->TDBF_Display_button ( 'cancel', 'class', 'annuler','reset' );
    
    $form->display();
}
