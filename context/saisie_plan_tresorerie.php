<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

/**
 * @todo Ajouter le système de détection du siren de l'entreprise (voir le début de la fonction d'affichage)
 * @todo Gérer mieux la date
 * 
 */
require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'interfaces/i_display.php';
require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'modules/Error_Manager.php';
require_once ABSPATH . 'interfaces/i_DB.php';
require_once ABSPATH . 'interfaces/i_calcul.php';
require_once ABSPATH . 'public/js/Load_Script.php';

/******************** TRAITEMENT DU FORMULAIRE DE SAISIE ********************/
if (isset($_POST['BTN_Valider']) && !empty($_POST['wording']) && !empty($_POST['account']) && !empty($_POST['amount'])) {
    $validation = new Data_Validation();

    $has_error = false;
    // On lance les validations
    if (!$validation->verifText($_POST['wording'])) {
        Error_Manager::getInstance()->addErrorInput(13, 'wording');
        $has_error = true;
    }
    if (!$validation->limitChara($_POST['wording'], 2, 100)) {
        Error_Manager::getInstance()->addErrorInput(4, 'wording');
        $has_error = true;
    }

    if (!$validation->verifNumber($_POST['account'])) {
        Error_Manager::getInstance()->addErrorInput(12, 'account');
        $has_error = true;
    }

    if (!$validation->verifNumber($_POST['amount'])) {
        Error_Manager::getInstance()->addErrorInput(12, 'amount');
        $has_error = true;
    }
    if (!$validation->positifNumber($_POST['amount'], true)) {
        Error_Manager::getInstance()->addErrorInput(12, 'amount');
        $has_error = true;
    }

    $estPrevisionnelle = ( $_POST['estPrevisionnelle'] == 'true' ) ? true : false;

    // A MODIFIER !!!!!!!
    $date = '2017-03-18';
    /////////////////////

    $msg = "Erreur de l'ajout de la saisie du plan de trésorerie";
    if ($has_error === false) {
        if (add_ecriture_comptable($_POST['wording'], $_POST['account'], $_POST['amount'], $date, $estPrevisionnelle, $_POST['entreprise']))
            $msg = "Ajout de la saisie du plan de trésorerie";
    }
    echo $msg;
}
display_form_saisie_plan_treso();
    
function display_form_saisie_plan_treso() {
    $form = new Form_Manager('#', 'class', 'post');

    // On remplis un tableau avec la liste des entreprises de l'utilisateur
    $entreprises = get_entreprises( get_ID_user() );
    $liste_entreprise = array();
    foreach($entreprises as $single) {
        $liste_entreprise[ $single['ID'] ] = $single['nom'];
    }
    $form->TDBF_Display_select( $liste_entreprise, 'Entreprise concernée', 'entreprise', 'class', Error_Manager::getInstance()->getErrorInput( 'entreprise' ));
    
    $form->TDBF_Display_date( 'Mois', 'date', 'input', Error_Manager::getInstance()->getErrorInput( 'date' ) );
    $form->TDBF_Display_text ( 'Libellé', 'wording', 'class','', Error_Manager::getInstance()->getErrorInput( 'wording' ) );
    
    // A MODIFIER !!!!!!!
    $SirenEntreprise = $entreprises[0]['SIREN'];
    //$SirenEntreprise = '987654321';
    /////////////////////
    
    $form->TDBF_Display_select ( get_account( $SirenEntreprise ) ,'Compte', 'account', 'class','', Error_Manager::getInstance()->getErrorInput( 'account' ) );
    $form->TDBF_Display_text ( 'Montant', 'amount', 'class','', Error_Manager::getInstance()->getErrorInput( 'amount' ) );
    $form->TDBF_Display_radio ( array('true' => "Prévisionnelle", 'false' => "Réel"), '', 'estPrevisionnelle','', Error_Manager::getInstance()->getErrorInput( 'amount' ) );
    
    $form->TDBF_Display_button ( 'BTN_Valider', 'class', 'Valider', 'submit' );
    $form->TDBF_Display_button ( 'BTN_Annuler', 'class', 'Annuler', 'reset' );

    $form->display();
}