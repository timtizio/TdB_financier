<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'config/dictionnary_error.php';
require_once ABSPATH . 'interfaces/i_DB.php';
require_once ABSPATH . 'modules/member_area_manager.php';
require_once ABSPATH . 'public/js/Load_Script.php';

Load_Script::getInstance()->enqueue_script( 'public/js/update_account.js' );
Load_Script::getInstance()->enqueue_script( 'public/js/add_facture.js' );


// On vérifie qu'on est pas la a cause d'une requete AJAX
if ( ! isset( $ajax ) )
    $ajax = false;

// si on à cliqué sur le bouton valider :
if( !$ajax && isset( $_POST['BTN_Valider'] ) && !empty( $_POST['wording'] ) && !empty( $_POST['account'] ) && !empty( $_POST['amount'] ) ){
    $validation = new Data_Validation();

    $has_error = false;
    // On lance les validations
    if( ! $validation->verifText( $_POST['wording'] ) ) { Error_Manager::getInstance()->addErrorInput(1, 'wording'); $has_error = true; }
    if( ! $validation->limitChara( $_POST['wording'], 2, 50 ) ) { Error_Manager::getInstance()->addErrorInput(2, 'wording'); $has_error = true; }
    
    
    if( ! $validation->verifNumber( $_POST['account'] ) ) { Error_Manager::getInstance()->addErrorInput(1, 'account'); $has_error = true; }
    if( ! $validation->limitChara( $_POST['account'], 2, 50 ) ) { Error_Manager::getInstance()->addErrorInput(2, 'account'); $has_error = true; }
    
     
    if( ! $validation->verifNumber( $_POST['amount'] ) ) { Error_Manager::getInstance()->addErrorInput(1, 'amount'); $has_error = true; }
    if( ! $validation->limitChara( $_POST['amount'], 1, 8 ) ) { Error_Manager::getInstance()->addErrorInput(2, 'amount'); $has_error = true; }

    
    $date = $_POST['date-annees'] . '-' . $_POST['date-mois'] . '-' . $_POST['date-jour'];

    if ( $has_error === false){
        if( add_ecriture_comptable( $_POST['wording'], $_POST['account'], $_POST['amount'], $date, false, $_POST['entreprise'] )) {
            echo "La facture a bien été ajouté";
        }
        else {
            echo"Erreur lors de l'ajout de la facture";
        }
    }
    else {
        echo "Erreur lors de l'ajout de la facture";
    }
}
if( ! $ajax )
    form_saisie_facture();

function form_saisie_facture() {
    $entreprises = get_entreprises( get_ID_user() );
    // A MODIFIER !!!!!!!
    $SirenEntreprise = $entreprises[0]['SIREN'];
    /////////////////////
    
    $form_saisie_facture = new Form_Manager('#', 'class', 'post', 'form-saisie-facture');

    
    // On remplis un tableau avec la liste des entreprises de l'utilisateur
    $liste_entreprise = array();
    foreach($entreprises as $single) {
        $liste_entreprise[ $single['ID'] ] = $single['nom'];
    }
    $form_saisie_facture->TDBF_Display_select( $liste_entreprise, 'Entreprise concernée', 'entreprise', 'class', Error_Manager::getInstance()->getErrorInput( 'entreprise' ));
    
    $form_saisie_facture->TDBF_Display_date_input("Date : ", "date", "date-picker");
    
    $form_saisie_facture->TDBF_Display_text ( 'libellé', 'wording', 'class','',Error_Manager::getInstance()->getErrorInput( 'wording' ) );
    $form_saisie_facture->TDBF_Display_select ( get_account( $SirenEntreprise ) ,'Compte', 'account', 'class','', Error_Manager::getInstance()->getErrorInput( 'account' ) );
    $form_saisie_facture->TDBF_Display_text ( 'montant', 'amount', 'class','',Error_Manager::getInstance()->getErrorInput( 'amount' ) );
    $form_saisie_facture->TDBF_Display_button ( 'BTN_Valider', 'class', 'valider','submit');
    $form_saisie_facture->TDBF_Display_button ( 'BTN_Annuler', 'class', 'annuler','reset' );
    
    $form_saisie_facture->display();
}
