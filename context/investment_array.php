<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . 'modules/display_manager.php';
require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'modules/Error_Manager.php';
require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'modules/member_area_manager.php';
require_once ABSPATH . 'interfaces/i_DB.php';
require_once ABSPATH . "public/js/Load_Script.php";
require_once ABSPATH . "config/dictionnary_error.php";

Load_Script::getInstance()->enqueue_script( 'public/js/update_account.js' );
Load_Script::getInstance()->enqueue_script( 'public/js/add_investment.js' );

if( isset( $_POST['BTN_OK'] ) && !empty( $_POST['name_investment'] ) && !empty( $_POST['amount_ht'] ) && !empty( $_POST['amount_ttc'] ) && !empty( $_POST['tva'] ) && !empty( $_POST['duree_investment'] ) ){
    $validation = new Data_Validation();

    $has_error = false;
    // On lance les validations
    if( ! $validation->verifText( $_POST['name_investment'] ) ) { Error_Manager::getInstance()->addErrorInput(15, 'name_investment'); $has_error = true; echo "1"; }
    if( ! $validation->limitChara( $_POST['name_investment'], 2, 50 ) ) { Error_Manager::getInstance()->addErrorInput(16, 'name_investment'); $has_error = true; echo "2"; }
    
    if( ! $validation->verifFloat( strval($_POST['amount_ht']) ) ) { Error_Manager::getInstance()->addErrorInput(17, 'amount_ht'); $has_error = true; echo "3"; }
    if( ! $validation->limitChara( $_POST['amount_ht'], 1, 8 ) ) { Error_Manager::getInstance()->addErrorInput(18, 'amount_ht'); $has_error = true; echo "4"; }
    
    if( ! $validation->verifFloat( strval($_POST['amount_ttc']) ) ) { Error_Manager::getInstance()->addErrorInput(17, 'amount_ttc'); $has_error = true; echo "5"; }
    if( ! $validation->limitChara( $_POST['amount_ttc'], 1, 8 ) ) { Error_Manager::getInstance()->addErrorInput(18, 'amount_ttc'); $has_error = true; echo "6"; }
    
    if( ! $validation->verifFloat( strval($_POST['tva']) ) ) { Error_Manager::getInstance()->addErrorInput(19, 'tva'); $has_error = true; echo "7"; }
    if( ! $validation->limitChara( $_POST['tva'], 1, 5 ) ) { Error_Manager::getInstance()->addErrorInput(20, 'tva'); $has_error = true; echo "8"; }
    
    if( ! $validation->verifNumber( $_POST['duree_investment'] ) ) { Error_Manager::getInstance()->addErrorInput(21, 'duree_investment'); $has_error = true; echo "9"; }
    if( ! $validation->limitChara( $_POST['duree_investment'], 1, 2 ) ) { Error_Manager::getInstance()->addErrorInput(22, 'duree_investment'); $has_error = true; echo "10";  }

    
    $msg = "Votre investissement n'a pas pu être ajouté";
    if( $has_error === false ) {
              
        // On calcul le nombre de mois
        if( $_POST['type_duree_amortissement'] === 'year' ) $nbMois = $_POST['duree_investment'] * 12;
        else $nbMois = $_POST['duree_investment'];
        
        // On définis le code de type d'investissement
        // 0=> 'linear' | 1=> 'degressive
        if( $_POST['type_amortissement'] === 'linear' ) $typeAmortissement = 0;
        else $typeAmortissement = 1;
        
        // A MODIFIER
        $date = "1996-11-14";
                
        if( record_investment( $_POST['name_investment'], $date, $_POST['amount_ht'], $_POST['tva'], $typeAmortissement, $nbMois, $_POST['entreprise'], $_POST['account'] )) {
            $msg = "Ajout nouvel investissement";
        }
    }

    echo $msg;
}
amortization_display();

function add_investisment(){
$amortization_grabing = new Form_Manager('#', 'add_investisment' ,'POST', 'form-saisie-investment');

// On remplis un tableau avec la liste des entreprises de l'utilisateur
$entreprises = get_entreprises(get_ID_user());
$liste_entreprise = array();
foreach ($entreprises as $single) {
    $liste_entreprise[ $single['ID'] ] = $single['nom'];
}
$amortization_grabing->TDBF_Display_select($liste_entreprise, 'Entreprise concernée', 'entreprise', 'class', Error_Manager::getInstance()->getErrorInput('entreprise'));

$amortization_grabing->TDBF_Display_date_input("Date : ", "date", "date-picker");

$amortization_grabing->TDBF_Display_text('Libellé', 'name_investment', 'name_investment');
$amortization_grabing->TDBF_Display_text('Montant TTC de votre investissement', 'amount_ttc', 'amount_ht');
$amortization_grabing->TDBF_Display_text('Montant HT de votre investissement', 'amount_ht', 'amount_ttc');
$amortization_grabing->TDBF_Display_text('TVA', 'tva', 'tva','');

// A MODIFIER !!!!!!!
$SirenEntreprise = $entreprises[0]['SIREN'];
/////////////////////
$amortization_grabing->TDBF_Display_select ( get_account( $SirenEntreprise ) ,'Compte', 'account', 'class','', Error_Manager::getInstance()->getErrorInput( 'account' ) );

$amortization_grabing->TDBF_Display_select(['linear' => 'Lineaire', 'degressive' => 'Degressif'], 'type d\'amortissement', 'type_amortissement', 'type_amortissement');
$amortization_grabing->TDBF_Display_select(['month' => 'Mois','year' => 'Année'], 'type de durée d\'amortissement', 'type_duree_amortissement', 'type_duree_amortissement');
$amortization_grabing->TDBF_Display_text('Durée de votre amortissement', 'duree_investment', 'duree_investment');


$amortization_grabing->TDBF_Display_button('BTN_OK', 'BTN_Valider', 'Enregistrer', 'submit');
$amortization_grabing->TDBF_Display_button('BTN_NOK', 'BTN_Annuler', 'Annuler', 'reset');

$amortization_grabing->display();
}


function amortization_display(){
    // On affiche les boutons du choix du tableau à afficher
    $amortization_array_display = new Form_Manager('#', 'amortization_display' ,'POST');
    $amortization_array_display->TDBF_Display_button_link('array_display_linear', 'index.php?p=investment-array&type=linear' , 'array_display', 'Tableau d\'amortissement linéaire');
    $amortization_array_display->TDBF_Display_button_link('array_display_degressive', 'index.php?p=investment-array&type=degressive' , 'array_display', 'Tableau d\'amortissement dégréssif');
    $amortization_array_display->TDBF_Display_button_link('new_investment', 'index.php?p=investment-array&type=addnew' , 'new_investment', 'Nouvel investissement');

    
    $amortization_array_display->display();
    
    // Si on a cliqué sur un bouton ...
    if( isset($_GET['type']) ) {
        if ( $_GET['type'] === 'linear' ){
            display_array_linear();
            return;
        }
        else if ( $_GET['type'] === 'degressive' ){
            display_array_degressive();
            return;
        }
        else if ( $_GET['type'] === 'addnew' ){
            add_investisment();
            return;
        }
    }

}
function display_array_linear() {
    echo "on va voir du linear !";
}

function display_array_degressive() {
    echo "on va voir du degressive !";
    
}


/*
$tableaux = array(
    array(
        'Année', 
        'Nombre d\'années restantes',
        'VNC Début',
        'Amortissement',
        'Cumul amortissement',
        'VNC Fin'
    ),
);
$annees = array(1,2,3,4,5);
$contenu1 = array('d','f','g','h','j');

foreach($annes as $annee) {
    array_push($tableaux, array());
}
for ($i = 0 ; $i < 5 ; $i++ ) {
    $line = array($annees[$i]);
    foreach($contenu1 as $content) {
        array_push($line, $content);
    }
    var_dump($line);
    array_push($tableaux, $line);
}

var_dump($tableaux);
$degressive_array = display_array($tableaux);
var_dump($degressive_array);
*/
