<?php
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname( dirname(__FILE__) ) . '/');

require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'config/dictionnary_error.php';
require_once ABSPATH . 'interfaces/i_DB.php';


//Si on clique sur Envoyer
if(isset($_POST['send']) AND !empty($_POST['mail'])){
    
    $validation = new Data_Validation();
    
    $has_error = false;
    $user_exist = get_id_user_by_mail($_POST['mail']);
    //var_dump($user_exist);
    if( ! $validation->verifEmail( $_POST['mail'] ) ) { Error_Manager::getInstance()->addErrorInput(3, 'mail'); $has_error = true; }
                
    
        //Si l'utilisateur existe
        if($user_exist != false){
            //création nouveau mot de passe
            $new_pass = GenereCode("alphanumerique", 10);

            //Enregistrement nouveau mdp
            $password = sha1($new_pass);
            load_new_pass($password,$user_exist);
            //echo 'nouveau mot de passe :'.$new_pass.'<br/>' ;
            
            //Envoi du mail
            $content = "<p>Vous avez fait une demande de changement de mot de passe.</p>";
            $content .= "<p>Le mot de passe que nous vous avons choisie est <strong>" . $new_pass . "</strong>";
            $content .= "<p>Nous vous invitons à vous connecter et changer ce mot de passe dans la rubrique \"Modifier ses données personnelles\"</p>";
            $content .= "<p>Toutes l'équipe vous souhaite une bonne continuation avec les services <a href='" . ADRESSE_SITE . "'>TDBF</a>";
            
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: timothe@simplon-ve-2017.fr' . "\r\n" .
                'Reply-To: timothe@simplon-ve-2017.fr' . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
            
            mail( get_mail_user( $user_exist ), 'TDBF - Votre nouveaux mot de passe', $content, $headers );
            
            //affichage du message d'envoi
            echo 'Un e-mail vient de vous être envoyé avec votre nouveau mot de passe.';  
        }
        else{
            echo 'l\'adresse mail n\'est pas valide!';
            //affichage du formulaire
            display_form_forget();
        }
}


else{
    //affichage du formulaire
    display_form_forget();
}

function display_form_forget(){
    
    $form = new Form_Manager('#', 'class', 'post');

    $form->TDBF_Display_text( 'Veuillez entrez votre adresse mail', 'mail', 'input','' );
    $form->TDBF_Display_button ( 'send', 'envoi', 'Envoyer', 'submit' );

    $form->display();
    }
    
    
    
/*
 * Permet de générer un code aléatoire
 * ARGUMENTS
 * $type 		:: string 	=> Determine le type de code à générer, prends l'un des valeurs suivante "numerique" 
 *                                          OU "texte" OU "alphanumerique"
 * $length		:: Int32	=> Détermine le nombre de caractères composants chaque code
*/
function GenereCode ( $type , $length )
{
 
	$code = "";
 
	switch ( $type )
	{
		case "numerique":
			for( $i=0; $i < $length; $i++)
			{
				$code .= chr(rand(48 , 57));
			}
		break;
		case "texte":
			for( $i=0; $i < $length; $i++)
			{
				$code .= chr(rand(65 , 90));
			}
		break;
		case "alphanumerique":
			$OdlValue = "";
			for( $i=0; $i < $length; $i++)
			{
				if ( ! is_int ($OdlValue) )  $OdlValue = ord( $OdlValue );
				if( ($OdlValue % 2) == 0 )
				{
					$OdlValue = chr(rand(48 , 57));
				} 
				else
				{
					$OdlValue = chr(rand(65 , 90));
				}
				$code .= $OdlValue;
			}
		break;
	}
	return $code;
}

