<?php
require '../modules/Form_Manager.php';
require '../interfaces/i_DB.php';

    
if( isset( $_POST['enregistrer_tuile'] )  ){
    $date_in = $_POST['date_in-annees'] . '-' . $_POST['date_in-mois'] . '-' . $_POST['date_in-jour'];
    $date_out = $_POST['date_out-annees'] . '-' . $_POST['date_out-mois'] . '-' . $_POST['date_out-jour'];
    add_custom_tuile( $_POST['Type_de_presentation'], $_POST['Field_selection'], $_POST['indicator_type'], $date_in, $date_out);
}

/**
 * fonction qui affiche le menu de personalisation
 * 
 * 
 **/
function select_dashboard_custom_tuile(){
    
    $Type_de_presentation = array(
        'chart' => 'Graphique',
        'clacos' => 'Camembert',
        'tableaux' => 'Tableaux',
        'frise' => 'Frise'
    );
    $Field_target = array(
        'total_encaissement' => 'dfsf' ,
        'total_decaissement' => 'xxvcv',
        'total_farine' => 'xcvx',
        'pourvoir' => 'cfb'
    );
    $indicator = array(
        'compte_de_resultat' =>'df75sf' ,
        'tresorerie' => 'df2sf',
        'BFR' => 'dfs7f',
        'pourvoir' => 'df78sf' 
    );
    
    $dashboard_custom_field = new Form_Manager("#", 'customize_dashboard_element', 'POST', 'customize_dashboard_element');
    $dashboard_custom_field->TDBF_Display_select($Type_de_presentation, 'Type_de_presentation', 'Type_de_presentation', 'Type_de_presentation');
    $dashboard_custom_field->TDBF_Display_select($Field_target, 'Champ cible', 'Field_selection', 'Field-selection');
    $dashboard_custom_field->TDBF_Display_select($indicator, 'Choix de l\'indicateur', 'indicator_type', 'indicator_type');
    $dashboard_custom_field->TDBF_Display_date_input('Début de la plage temporelle', 'date_in', 'date_in', 'date_in');
    $dashboard_custom_field->TDBF_Display_date_input('Fin de la plage temporelle', 'date_out', 'date_out', 'date_out');
    $dashboard_custom_field->TDBF_Display_button('enregistrer_tuile', 'enregistrer_tuile', 'Enregistrer la tuile','submit');
    $dashboard_custom_field->TDBF_Display_button('annuler', 'annuler', 'annuler','reset');
    
    echo '<div class="custom_dashboard">
    ';
    $dashboard_custom_field->display();
    
    echo '
    </div>';
}

//select_Dashboard_custom_tuile();
get_tuile(1);

function Dashboard_display(){
    $Dashboard_display = new Form_Manager("#", 'add_dashboard_element', 'POST', 'add_dashboard_element');
   

    $Dashboard_display->display();
}

