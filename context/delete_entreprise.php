<?php

require_once ABSPATH . "modules/Error_Manager.php";
require_once ABSPATH . 'modules/Form_Manager.php';
require_once ABSPATH . 'modules/Data_Validation.php';
require_once ABSPATH . 'interfaces/i_DB.php';



function display_form_supp_entreprise() {
    $form = new Form_Manager('#', 'class', 'post');

    // On remplis un tableau avec la liste des entreprises de l'utilisateur
    $entreprises = get_entreprises( get_ID_user() );
    $liste_entreprise = array();
    foreach($entreprises as $single) {
        $liste_entreprise[ $single['ID'] ] = $single['nom'];
    }
    $form->TDBF_Display_select( $liste_entreprise, 'Entreprise concernée', 'entreprise', 'class', Error_Manager::getInstance()->getErrorInput( 'entreprise' ));
    $form->TDBF_Display_button ( 'send', 'class', 'Supprimer', 'submit' );
   
    $form->display();
}


//Si on clique sur Supprimer
if(isset($_POST['send']) AND !empty($_POST['entreprise'])){
    
    $id_entreprise = $_POST['entreprise'];
    
    $has_error = 0;
    if( supp_liaison( $id_entreprise ) ) $has_error = 1 ;
    if( supp_ecriture_comptable( $id_entreprise ) ) $has_error = 2 ;
    if( supp_plan_comp_perso( $id_entreprise ) ) $has_error = 3 ;
    if( supp_ammortissement( $id_entreprise ) ) $has_error = 4 ;
    if( supp_entreprise( $id_entreprise ) ) $has_error = 5 ;
    
    if ( $has_error != 0 ) {
        echo "Erreur lors de la suppression de l'entreprise";
        trigger_error( "Erreur lors de la suppression de l'entreprise d'ID" . $id_entreprise . " (\$has_error = " . $has_error . ")" );
    }
    else echo "L'entreprise a bien été supprimer"; 
}

display_form_supp_entreprise();